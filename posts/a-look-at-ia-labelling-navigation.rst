.. link: 
.. description: A look into some basic Information Architecture questions and concepts.
.. tags: Information Architecture, labelling, navigation, Content Management
.. date: 2013/08/22 20:42:13
.. title: A Look at Information Architecture: Ordering, Labelling and Navigation Systems
.. slug: a-look-at-information-architecture-ordering-labelling-navigation

.. This defines a literal break, so we can do a simple unstyled semantically incorrect list.
.. |br| raw:: html

   <br />

An Information Architect is somebody who organises information. The `course text <http://shop.oreilly.com/product/9780596527341.do>`_ for `3623ICT <https://courseprofile.secure.griffith.edu.au/student_section_loader.php?section=1&profileId=72815>`_ uses the phrase "Internet librarian" at one point - which I feel really helps the average person grok the meaning of the title, although technically an Information Architect doesn't always work on Internet connected content. 

.. figure:: /blog_images/contextuserscontent.jpg
        :align: center
        :alt: Information Architecture views: Users, Context and Content.

        The Information Archictecture Summoning Circle. Just add goat.

In this post I describe the role an Information Architect plays and touch on some of the problems they may deal with and conclusions they may come to.

.. TEASER_END

An Information Architect differs from a Librarian in that they have more challenges to deal with and that they'd rarely deal with `dead tree format <http://en.wikipedia.org/wiki/Hard_copy>`_. Like a Structural Architect, they have to design systems that suit a wide range of situations - rather than the somewhat static, well defined and typical Library universe of discourse that a Librarian deals with. A restaurant building is different from a school building and a blog site is different from a product sales and support site.

A particular universe of discourse can be described by considering the three different sections in the diagram above which shows the views an architect (or designer) must consider for any situation. You could come up with analogies for the concept of dealing with the other parts of an Information Architect's job (search & navigation systems, thesauri definition, taxonomy and metadata, etc.) but I assume there's no need to.

It is typical to find Information Architects working on larger websites, and perhaps just Web Designers who understand the basics of Information Architecture on smaller sites. Though, there is no reason a dedicated Information Architect couldn't be hired in a consulting capacity on a smaller site.

Whether a full-time Information Architect or not, people in this role working on websites design the following:

* Organisation systems
* Labelling systems
* Search systems
* Navigation systems
* Thesauri, Controlled Vocabularies, and Metadata

Let's have a look at a basic Information Architecture problem...

Sorting
=======

This is a list of a different things in no particular order. 

        **Unsorted List**

        El Paso, Texas |br|
        Saint Nicholas, Belgium |br|
        *The Lord of the Rings* |br|
        Newark, New Jersey |br|
        *XVIIme siècle* |br|
        *.38 Special* |br|
        St. Louis, Missouri |br|
        New York, New York |br|
        *1001 Arabian Nights* |br|
        *The 1-2-3 of Magic* |br|
        Albany, New York |br|
        *#!%&: Creating Comic Books* |br|
        The Hague, Netherlands |br|
        *$35 a Day Through Europe* |br|
        *H20: The Beauty of Water* |br|
        Plzen, Czech Republic

To sort this list, we can approach it through *Exact Organisation Schemes* which include *alphabetical/alphanumeric* (in order of number/letter), *chronological* (in order of time) and *geographical* (by location). The simplest would be alphanumerical "dictionary sort" in ascending order. I sorted the following with GNU Sort's dictionary sort (*sort -bdf*).

        **Dictionary Sort** 

        *1001 Arabian Nights* |br|
        *$35 a Day Through Europe* |br|
        *.38 Special* |br|
        Albany, New York |br|
        *#!%&: Creating Comic Books* |br|
        El Paso, Texas |br|
        *H20: The Beauty of Water* |br|
        Newark, New Jersey |br|
        New York, New York |br|
        Plzen, Czech Republic |br|
        Saint Nicholas, Belgium |br|
        St. Louis, Missouri |br|
        *The 1-2-3 of Magic* |br|
        The Hague, Netherlands |br|
        *The Lord of the Rings* |br|
        *XVIIme siècle*

Let's consider some problems with this blind alphanumerical dictionary sort:

* Place names with prefixes such as *The* and *El* get sorted by these prefixes first and then the actual name. To this effect *The Hague* gets sorted under *T* rather than the secondary, possibly more meaningful and useful *H*. Likewise *El Paso* gets sorted under *E* rather than *P*. This problem makes this *"exact"* organisation scheme a bit ambiguous and the solution may be to simply ignore this problem if content isn't too clustered around common prefixes.

* *Newark, New Jersey* gets sorted before *New York, New York* because the *a* in *Newark* immediately places it above the *Y* of the same position in New York.

* *Saint Nicholas, Belgium* is before *St. Louis, Missouri* because the *a* in *Saint Nicholas* is sorted before the *t* in *St. Louis*. GNU Sort does not take into account that *St.* and *Saint* are the same thing, else they would have their positions swapped. Similarly *XVII* in *XVIIme siècle* is not taken to be the more literal *17* instead and ends up at the end the list instead of the beginnig.

* Numbers are handled as the precursors to letters. If you had *1st Birthday*, *2nd Birthday* and *Third Birthday* they would appear to be sorted in that order.

* Punctuation and special characters are basically the same thing, however it is worth noting that the options I specified to GNU Sort told it to ignore the space if it was the first character. GNU Sort orders these characters by their position in the `ASCII Table <http://www.asciitable.com/>`_. The sorting options I used also ignored capital letters - if you look at the ASCII table linked above you can see that this makes a difference to the program as capital letters appear before lower case. Having capitals appear first may be desirable in some cases, it may be a quick way of separating out `Proper Nouns <https://en.wikipedia.org/wiki/Proper_noun>`_ in English texts (though obviously error prone). 

* If we assume that the italicsised items are book titles there is a more useful way to organise this list such as books first. However if there is a book with a name that is also a location this scheme could get confusing due to insufficient formatting a metadata to clearly express the difference.

* If the cities represent places you’ve visited and the book titles are ones you’ve read, chronology could be used to represent a timeline of these events. Though, it is worth noting that events can overlap with this scheme and people in the original set are discarded..

The following table represents places visited and books read over time. In intervals where books were read and places were visited in the same year I put the books last in alphanumeric order.

====== ======================================================================
Year   Event(s)
====== ======================================================================
1991   *$35 a Day Through Europe*
1992   *.38 Special*
1993   Albany, New York; *#!%&: Creating Comic Books*
1994   El Paso, Texas; *H20: The Beauty of Water*
1995   Newark, New Jersey; New York, New York; Plzen, Czech Republic
1996   The Hague, Netherlands; *The 1-2-3 of Magic*
1997   *The Lord of the Rings*; *XVIIme siècle*
====== ======================================================================

The above table makes sense if you regard reading a book and visiting a place as just generic *events*, but an even more useful representation of the data would be the books read in those cities regardless of year, but in chronological order. Andreas Lukita has suggested this in `his blog <http://icmandre.blogspot.com.au/2013/08/information-architect-alphabetizing.html>`_.

Labelling and Navigation
========================

Once a system for organising content logically is established, the next step is to implement a labelling and navigation scheme.

.. Slides for my template system are a godawful hack a the moment. Turns out I don't want slides here anyway...
.. figure:: /blog_images/ddevnet_nav.jpg
        :align: center
        :alt: A screen-shot of http://ddevnet.net

        My main personal website: `DDEVnet.net <http://ddevnet.net>`_

The following table describes the coherence (or lack of) in the labels and navigation of `DDEVnet.net <http://ddevnet.net>`_.

============================= =========================================== =================================================
Label                         Destination's Heading Label                 Destination's <title> Label
============================= =========================================== =================================================
**Top of Page Labels**                                                                                         
DDEVnet.net                   DDEVnet.net                                 DDEVnet.net | DDEVnet.net 
Blog                          \-                                          DDEVnet.net | DDEVnet.net
Articles                      Tags                                        Tags | DDEVnet.net
RSS                           DDevnet.net                                 DDEVnet.net
About                         About                                       About | DDevnet.net
**Body Navigation Labels**
New New Server                New Site New Server                         New Site New Server | DDEVnet.net
*[Article Name]*              *[Article Name]*                            *[Article Name | DDEVnet.net]*
**Footer Navigation Labels** 
Daniel Devine                 \-                                          \- [Mail-to dialog, rather than contact page]
Nikola Powered                Nikola                                      `Nikola | Nikola <http://nikola.ralsina.com.ar/>`_
Flattr Me                     Daniel Devine                               `Daniel Devine (DDevine) - Flattr <http://flattr.com/profile/DDevine>`_
============================= =========================================== =================================================

There are a few problems that have become apparent in this table. Because this is my site I know why they are this way - inflexiblity of the Nikola static site generator, at least the version the site is based on anyway. For example the Blog page's Title should be "Blog | DDEVnet.net" however it isn't due to a quirk to do with making the post summary page (aka. the Blog page) not the main page for the site. I will query the Nikola `developer <https://github.com/ralsina>`_ about ways to fix this.
The Articles page has the heading and title "Tags". This is because I could not get Nikola to do a nice listing of articles and so I decided to link to the tags summary for the set of posts of the Article subtype. It is like I could write some custom templates to fix this. Finally the last unsatisfactory item is my contact link in the footer. Using mailto: links is an archaic idea and I really should link to the Contact part of the About page instead. 

Throughout the site there are a few inconsistencies in labelling. The post listing on the main page is not formatted the same as the listing on the Blog page and is missing some fields (date, number of comments). I can fix the code I use to get the listing on the front page to make it be the same as the listing on the Blog page. Although it is consistent, there are some pages where the headings are not also links - these appear in orange and when you have a page which has both styles of heading it can look a bit odd. On the About page the "Source" link for the article appears next to the search box and it appears to be the label for the input field - this needs to be fixed. It would make sense to move the Source label out of the header (maybe into the footer). The About page should not have a Source label - I may be able to disable it for that page. Thankfully the state of the post and article pages is quite satisfactory appart from the aforementioned positioning of the "Source" label.

None of the above issues will bother (or be particularly noticed) by my target audience who are used to trudging through terrible websites; but usually arrive directly at a page via search engine). However fixing the Articles section will increase the chance of users who browse the site to discover other content they may be interested in.

Sites in the style of `DDEVnet.net <http://ddevnet.net>`_ used to be very popular a few years ago before social networking sites exploded in popularity and the majority of the tech community became largely apathetic in regards to personal online presence (I'm resisting a rant; perhaps I'll expand on this later). In my immediate social circle I was unable to easily find a site comparable to my own, though I am sure they are there if I look hard enough. Pressing out into the edges of my social circle I found that `Adrian Sutton <http://www.symphonious.net/>`_ a fellow (but non-current?) `HUMBUG <http://humbug.org.au>`_ member has a blog - but what makes it actually similar to mine and not just a blog is that he has a clearly shown About page and also a Résumé. More similar to DDEVnet.net than Adrian's is that of `Chris Neugebaur <http://chris.neugebauer.id.au/>`_ because his main page is similar to mine in that it has a quick summary of who he is and what the site is, with a listing of recent blog posts and additional sections including an About linked off to the side. It is hard to say definitively whose is better but if I had to rank them I would say My site, Chris's and then Adrian's - though I feel a truly excellent site would combine aspects of all three. My site should include a Month/Year archive of blog posts like Chris's and it should contain a clear link to my résumé like Adrian's.

If you have a website I can recommend reviewing it as I have in this section - you may be surprised to find mistakes you assumed you were too clever to make!

.. figure:: /blog_images/smug-ia-bear.jpg
        :alt: The bear on the course textbook cover is super smug.
        :align: center

        Smug IA Bear is so smug. 
