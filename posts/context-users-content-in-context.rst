.. link: 
.. description: 
.. tags: Information Architecture, Design, Content Management 
.. date: 2013/09/10 13:07:17
.. title: Context, Users and Content in Context
.. slug: context-users-content-in-context

When designing an website it is imperative that some basic Information Architecture principles are taken into account. Our good friends Context, Users and Content need to be considered if you are to have a effective website that is worthy of your audience's time and your effort (and money).

.. Insert collage image of design documents here...

In this post I'll put into practice some concepts and tasks that are employed in the research and strategy development stages of a website design. I hope this practical context can help you can grasp what considerations are required and how to go about expressing them.

.. TEASER_END

.. Image of IA project lifecycle from course text (shows research and strategy development stages).

Sites which are business, product or education focused (aka. "Informational sites") are easiest to express the following techniques with. The subject I am choosing is topical at the time of this writing. Obviously the first consideration up is the aim:

Aim
----
    To create a website to communicate basic Australian voting and governance concepts to your "Average Joe" citizen in the simplest and most succinct way.

    The website aims to express the following things:

    * The Preferential Voting system.
        * How votes are counted and how preferences come into play.
        * Why there are no "Wasted Votes".
    * What the House of Representatives and The Senate are.
        * What the two houses of parliament do.
        * How and why power is/should be balanced within/between houses.
    * Simple things a voter can do to vote most effectively.
        * Tips for making candidate/party quick and painless.
        * List some useful sites/tools.
        * A short list of questions to ask about a candidate/party.

A statement which describes the aim may be given to you by the client, but you may also have to write one if you also take on the task of helping the client express their requirements. Writing one or two sentence paragraph to explain why you are making a website is a great way to start your design process as it helps form a picture of the context for your design decisions. The context is interdependent on your users, so the next thing to start defining is your audience profile:

Target Audience
----------------

    The target audience are "ordinary" voting Australians who it is assumed generally have a poor understanding of Australia's voting and governance models. The target audience's age ranges from the late teens (16-18) to 45 years of age, of all sexes. It is assumed that the target audience have only a fleeting interest in the subject and so information must be delivered in simple, quick and amusing way. The use of heavily summarised/simplified information, infographics and tip notes is expected. The socio-economic bracket to be focused on is the low to higher-middle income/class. It is expected that the site will also be used by those who already know the content but want to help share what they know with others.

The Target Audience section describes the characteristics of the anticipated audience such as:

* Gender
* Age
* Demographic
* Socio-economic background
* Role (eg. Producer, Consumer, Voter, Candidate)

Some extra things included are the target audience's interest level in the topic and how that is likely to influence the content design. (ie. "short, sweet and amusing" presentation of relatively "boring" content.)

Site Structure/Map
-------------------

From the Aim and the Audience Profile sections we now have a vague idea of how the content should be organised, which influences the navigation style you choose. For sites which require different views for different parts of the target audience (eg. Students vs. Teachers) you may have to strictly partition the content, however for our site our two groups of viewers ("informed citizens" and "uninformed citizens") do not require segregated views. There are two immediately obvious options for the design of the site:

* One long page which the user can scan over and pick up information quickly. The information is organised somewhat linearly on the page with "Guide" style of navigation. `Linear/Guide organisation example here <http://howfastisthenbn.com.au/>`_.
* A simple two-level hierarchy of content with a "Global" style navigation interface. This is a more organised approach but is a bit less friendly to those who wish to quickly scan the content. There are some design tricks that can be used to facilitate fluid navigation and content consumption as seen in this `simple hierarchy example <http://www.iwantmynbn.com.au>`_ such as integrating contextual navigation.

.. raw:: html

    <div style="float: left; margin-left:auto; margin-right: auto; width: 100%;">

.. figure:: /blog_images/howfastisthenbn.jpg 
    :align: left
    :width: 380px

    `HowFastIsTheNBN.com.au - Guide style <http://howfastisthenbn.com.au>`_

.. figure:: /blog_images/iwantmynbn.jpg
    :align: right
    :width: 380px

    `IWantMyNBN.com.au - Heirarchical <http://iwantmynbn.com.au>`_

.. raw:: html

    </div>

Given that our topic has convenient places to split content into different pages we will do so. This allows people to easily share a specific page when trying to explain a particular concept to somebody. The advantages of splitting content into pages is demonstrated in the `simple hierarchy example <http://www.iwantmynbn.com.au>`_ which it shows that it is possible to make a site of this structure reasonably easy for the audience to scan through the content with the use embedded contextual navigation close to the user's focus (within the `"F pattern" <http://webdesign.tutsplus.com/articles/design-theory/understanding-the-f-layout-in-web-design/>`_); encouraging them to move onto further content.
For the example site we'll use a simple hierarchy with a global navigation interface and embedded contextual navigation between related content.

Blueprint Diagrams
===================

Now we have decided on what style of structure our site will be in we can build a blueprint to express how our content will be organised and how the user may navigate the content. To create these you can use a program for creating flowchart diagrams such as LibreOffice Draw. Using a specialised program gives you the advantage of being able to easily iterate on the design without spending a lot of time manually redrawing or re-arranging the digram.

.. image:: /blog_images/blog3-blueprint.jpg

It's worth noting that this site does not have any repositories (that is: databases, or collections) where as you are likely to have them. You should indicate that these exist in your blueprint. In my case however I mention Twitter as a submission method for questions, but I don't think that this qualifies as an actual repository as content is not stored/retrieved from it.

.. Insert Global and Contextual navigation diagrams.

Wireframes
===========

Wireframes allow you to express how the content and navigation will be arranged and describe a user interface from an Information Architecture perspective. Wireframes don't require a large investment of time or skills to create.

.. image:: /blog_images/homepage.jpg

.. image:: /blog_images/contentindexpage.jpg

.. image:: /blog_images/contentpage.jpg

.. image:: /blog_images/faqpage.jpg

The above were made in LibreOffice Draw, but any modern word processor should do the job.

.. TODO bring in content from Morville

Controlled Vocabulary
======================

.. Metadata Matrix, controlled vocabulary.
.. Describe stuff about terminology, variations on terminology and the level of maintenance required to keep the content using the vocabulary curated. (Tables)

Controlled vocabularies are important for managing the accuracy of search results.

.. csv-table:: *Metadata Matrix*
    :header: "Vocabulary", "Description",  "Examples", "Maintenance"
    
    "Party name", "A name for a group of people with similar opinions who form policies", "Australian Labor Party (or ALP), Liberal National Party (or LNP)", "Difficult"
    "House", "A name for governmental organisational unit", "Upper, Lower, House of Representatives, Senate", "Easy"
    "type: Member", "A name for the type of member of government", "Senator, Minister, Prime Minister, Leader", "Moderate"
    "name: Member",  "A name for the member of government", "Tony Abbott, Julia Gillard, Robbo Da Yobbo"
    "Economic class", "A name for the ranking of people in relation to economic status", "High, Middle, Low", "Easy"
    "type: District", "A type of area", "Electorate, State, Region", "Easy"
    "name: District", "A name for a district", "Western Sydney, Griffith, Queensland", "Difficult"
    "group: Vote", "A name for the socio-economic group for which a campain is being targeted at", "City, Rural, Bogan, Battler, Family, Youth", "Moderate"
    "type: Vote", "A name for the governmental organisational unit which a vote is counted for", "Senate, House of Representatives, State, Shire", "Easy"

Try to think about the different ways somebody may try to describe the same thing. In `Information Architcture for the World Wide Web <http://shop.oreilly.com/product/9780596527341.do>`_ (the course text for 3623ICT) a particular example is shown where user types "pocketpc" into a store search and nothing comes up... Yet when the user searches "Pocket PC" multiple things show up. Also keep in mind that synonyms can get out of control or be handled badly. Perhaps a search for "Pocket PC" may bring up pants and desktop computers rather than `the actual gadget <https://en.wikipedia.org/wiki/Pocket_pc>`_.

Any vocabularies which are high (or difficult) maintenance are an indicator that a strategy for managing this complexity within the site is needed. If there is a way to automate the management of these vocabularies it should be considered. Ways to automate the management of a vocabulary may be to utilise a maintained 3rd party database or to set up a notification system to send alerts when the need for a change is detected.

With these basic principles under your belt you should be ready to start building sides which embody the basics of Information Architecture. 
