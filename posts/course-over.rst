.. link: 
.. description: I have completed 3623ICT, so bye for now! 
.. tags: 
.. date: 2013/11/11 14:51:48
.. title: Course Over
.. slug: course-over

I have completed 3623ICT and I sat the final exam last week. I am quite sure I passed.

If I do have more to say on Content Management or Information Architecture I may post it here (or not). My main blog is over at `DDEVnet.net <http://ddevnet.net>`_.

This site will stay up for the forseeable future as the upkeep of this site is basically non-existent (yay for statically compiled sites!). I cant' gaurantee that the comments will be persistent as the system that handles them is out of my control.

I tried to write my posts in a generic manner in the hope that they would be useful to somebody in the future! Feel free to link to them.
