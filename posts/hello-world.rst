.. link:
.. description: An introduction.
.. tags:
.. date: 2013/08/13 11:11:42
.. title: Hello World
.. slug: hello-world


I am a Bachelor of Engineering (Software Engineering) student at Griffith University in Brisbane, Australia. I am in my 3rd year, but how long until I graduate is a little tricky to judge because unlike many of my peers I'm not in a race to graduate. I do 30 Credit Points of courses per semester in the order I find convenient. I chose to do university not because I had no idea what else to do or because it was expected of me, but because it is a helpful tool to aid me with my personal interests however, don't get me started on the *design* of this particular tool (hint: It's broken).

.. TEASER_END

The reason for the creation of this blog is for the 3623ICT Information and Content Management course. I have started this course two and a half weeks late because at the time I was choosing my courses I opted to do Microprocessor Techniques - a course that I thought was obviously *infinitely more useful and relevant to me* at the time. After two weeks I decided to act decisively and drop Microprocessor Techniques as I was being haunted by the pains of an academic `RSI <https://en.wikipedia.org/wiki/Repetitive_strain_injury>`_. 3623ICT should be a fairly therapeutic change of habit that will hopefully allow me to recover for the next semester and allow me to be more effectively critical of poor Information Architecture; a subject I am looking forward to ranting about.

I hope to write these blog posts in a style which means that I can leave this blog up after the course is finished, or I can at least re-publish most of the content on my personal blog. I really deplore the amount of effort wasted on silly academic exercises so I try engaging in such exercsies when I can - and thus the reason for my blogging style.

On another note, hopefully I am not breaking the system by using my preferred blogging platform `Nikola <http://nikola.ralsina.com.ar>`_ which is a static site/blog generator written in my preferred language (`Python <http://python.org>`_) and hosted on my `own server <http://www.ransomit.com.au/vps>`_. I have run a few blogs over the years so I have come to form a solid opinion on merits and pitfalls of many blogging platforms. Since the rise of social networks my blogging has become a lot less frequent. Microblogging is starting to drop off in favour of approaches `less restricted by legacy requirements <http://autonomo.us/2013/02/11/fosdem-replace-legacy-communications/>`_ and so I predict blogs will rise again as a feature integrated into the new wave of social networks.

To complete my introduction I am to mention something about where I am heading. I have been working to strengthen my skills in software development in the interest of working on Free/Open Source Software. Whether I am employed or not in the `F <https://en.wikipedia.org/wiki/Free_and_open_source_software>`_/`OSS <https://en.wikipedia.org/wiki/Open-source_software>`_ development role is inconsequential as it is still my reason for my university education. However it is worth noting that F/OSS runs the greater portion of the Internet and also underpins the functionality of many consumer hardware devices so I like to think that employment that is somewhat related to my interests is almost unavoidable.

By the way, my email is daniel.devine2 {at} griffithuni.edu.au.
