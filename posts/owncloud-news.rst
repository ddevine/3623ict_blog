.. link: 
.. description: A review of ownCloud News, my self-hosted feed reader. 
.. tags: RSS, feeds, ownCloud
.. date: 2013/08/13 13:32:47
.. title: ownCloud News
.. slug: owncloud-news

I have been using various feed readers on and off for a few years. Most of the time my feed reader usage stopped when my morning routine changed or when I reformatted my computer and forgot to back up my reader; or sometimes both. Of course I could have used an agile and dependable cloud based solution like the defunct Google Reader, like every other pleb, but I rather not have a profile built on my reading habits *thank-you-very-much*. I started running `ownCloud <http://owncloud.org>`_ and a while later `ownCloud News <http://apps.owncloud.com/content/show.php?content=158434>`_ became available and so I eschewed the encumberments of a local feed reader. So what is ownCloud News and why do I use it? 

.. TEASER_END

.. figure:: /blog_images/owncloud1.jpg
   :alt: My ownCloud News instance.

   My ownCloud News instance.
   
ownCloud News is a fairly new app (first released in April 2013) which runs within `ownCloud <http://owncloud.org>`_ which is a free (as in freedom) web application which you can run (or chose an existing provider) to share and sync your files, calendars, contacts and also provide other applications such as an email reader or in our case a feed reader. Running ownCloud means that you can keep control of your data and privacy by hosting it under your own terms. You can customise, use and abuse ownCloud to your heart's content (well, at least within the limits of the `AGPL <http://www.gnu.org/licenses/agpl-3.0.html>`_). Also, if you decide you want to move your data to a different ownCloud provider (or even totally away from the ownCloud platform) then it should be easy as ownCloud is focused on open data formats and protocols and is somewhat resistant to proprietary lock-in due to the aforementioned AGPL license.

ownCloud News is installable via the ownCloud administration interface by simply clicking install on the News application from the list. There are some manually configurable things you may want to look at for a better/more efficient setup. An icon will appear in the bar on the left hand side and you can click the *+* icon to start adding RSS, Atom or even RDF feeds (as I found when adding the `Planet RDF <http://planetrdf.com/>`_ feed). Although I don't, you can organise your feeds into folders. If you are a Google Reader refugee (or from another platform that supports OPML) you can import your old feeds into ownCloud by clicking the configuration icon in the bottom left and importing your old manifest.

.. figure:: /blog_images/owncloud2.jpg
    :alt: The ownCloud News configuration menu.

    The ownCloud News configuration menu.

Using a feed reader can be a great way to keep on top of news, especially for things which you are generally too busy to manually go find out about. For some professions a feed reader can be an invaluable tool as it may help them be situationally aware. For example a Journalist needs to know what is going on and what the current tone of the media is and a programmer may need to know what the latest technologies, issues and exploits are. A feed reader can get everything new that you need to know all in one place so you can find it and digest it quickly. New items also increment a counter which indicates to the user when feed or category of feeds in a folder has fresh content and the counter decrements as you read the items, so if you are reading while on the go you can pick up where you left off. 

The real downside to feed readers is for procrastinators. A feed reader makes you too efficient and doesn't allow you to spend hours on end surfing sites or refreshing pages.

**Update** Since I originally wrote this I have found myself using feeds a lot more. I now have 20+ feeds in ownCloud News.
