.. link: 
.. description: A look at the Terms and Conditions and Privacy Policy for the legacy social network: Twitter.
.. tags: terms and conditions, privacy, legacy social media
.. date: 2013/08/16 13:39:21
.. title: Terms and Conditions & Privacy Policy: Twitter
.. slug: tc-privacy-twitter

.. These are roles that create classes for styling our concern levels. (highlight colours.)
.. role:: hblue
.. role:: hgreen
.. role:: hyellow
.. role:: horange
.. role:: hred

As part of 3623ICT Information and Content Management we are required to read the Terms and Conditions and Privacy Policies for a social network. What I found turned out to be mildly interesting (and a pain to write up) as you can see from the source file linked at the end. Twitter's T&C and Privacy Policy reveal the good, bad, ugly and interesting aspects of their network.

.. TEASER_END
 
Originally I was intending to write this post based of the `Identi.ca <https://identi.ca>`_ Terms and Conditions and Privacy Policy however when I went to go find these documents I found that they had disappeared! I am reasonably sure that back when Identi.ca ran the StatusNet platform rather than Pump.io these documents did exist. I guess back then `Evan Prodromou <https://e14n.com/evan>`_ had a much different strategy back then than with now current Pump.io and E14N, and the Identi.ca instance is not open for registration so it is all people who "know the score" so to say. I then went to try to use BuddyCloud instead - but I found they had the exact same problem. This is fairly concerning given that much of the premise behind the existence of distributed and federated social networks is *picking your provider based on Terms & Conditions and Policies that you agree with*!

.. figure:: /blog_images/twitterbluebird.jpg
        :align: center

        `Tweet. (Source) <http://stuffpoint.com/beautiful-birds/image/38356/tiny-blue-bird-picture/>`_

And so I was forced to go with the legacy social networking provider, `Twitter <http://twitter.com>`_, instead.

Let's Read This Thing: Interpretive Dance
--------------------------------------------
This key allows you to follow my thinking at the time of the reading of the particular term. It does not necessarily represent the seriousness.

The Key of Concern: 

:hblue:`Other (Blue)`

:hgreen:`No Concern/Positive (Green)`

:hyellow:`Mild/Medium (Yellow)`

:horange:`High (Orange)`

:hred:`Very High (Red)`

Terms and Conditions
^^^^^^^^^^^^^^^^^^^^^
`Twitter's Terms and Conditions (or Terms of Service) can be found here... <https://twitter.com/tos>`_

        * The T&C seem to cover basically everything Twitter. Not just the utility.

        1. Basic Terms
                * **Tip:** :hgreen:`You're responsible for what you put into Twitter. (People will see it!)`
                * :hgreen:`You must be able to agree to the Terms and Conditions, else don't use Twitter.`
                * :hgreen:`Your local laws may restrict your use of Twitter.`
                * :horange:`T&C may change without notice.`
                * :hyellow:`Twitter reserves to restrict or stop providing you service, without notice.`

 
        2. Privacy
                * **Tip:** :hgreen:`You can opt out of Twitter communications (emails)`
                * :hgreen:`When you give information of any kind to Twitter it is subject to their Privacy Policy`
                * :hyellow:`You are consenting to have your information stored and processed, and this may be in the United States.` (can of worms!)

        3. Passwords
                * :hgreen:`You are responsible for safeguarding your password.` (on your end? on theirs?)
                * :hgreen:`You're encouraged (not required) to use a "strong" (they provide reasonable definition, excluding length) password.`
                * :hyellow:`Twitter takes no responsibility for loss or damage for failing to comply with the above...` But what if THEY fail to protect your password? Do they even salt?

        4. Content on the Services
                * :hgreen:`You're responsible for any content you put on Twitter.`
                * :hgreen:`Twitter holds no responsibility to monitor content going in...`
                        * :hyellow:`Twitter *"may not"* monitor content... But they *can* monitor the content. (Ambiguity of the word "may").`
                * :hgreen:`Any use or reliance on content in Twitter is at your own risk.`
                * :hred:`"We do not endorse, support, represent or guarantee the completeness, truthfulness, accuracy, or reliability of any Content or communications posted via the Services or endorse any opinions expressed via the Services."` **People may lie on the Internet! Yes really!**
                * :hgreen:`You may see things on Twitter that you don't like. You may be tricked into seeing them.`
                * :horange:`Twitter makes no guarnatee and takes no responsibility for ensuring the the content you submit is actually expressed the way you intended it to be.`

        5. Your Rights
                * **Tip:** :horange:`"This license is you authorizing us to make your Tweets available to the rest of the world and to let others do the same."`
                * **Tip:** :hyellow:`"Twitter has an evolving set of rules for how ecosystem partners can interact with your Content. These rules exist to enable an open ecosystem with your rights in mind. But what’s yours is yours – you own your Content (and your photos are part of that Content)."`
                * :hgreen:`You retain the rights to the content you submit`
                * :hred:`You grant Twitter "a worldwide, non-exclusive, royalty-free license (with the right to sublicense) to use, copy, reproduce, process, adapt, modify, publish, transmit, display and distribute such Content in any and all media or distribution methods."`
                * :horange:`Twitter can use your content for promotion (but this was written strangely)`
                * :horange:`Twitter can "provide" your content to other organisations, who may republish your content.`
                * :hred:`Twitter can relicense the content it provides to its partners.`
                * :hred:`You may not recieve compensation for your content that Twitter uses or provides to it's partners.`
                * :hgreen:`Twitter may adapt your content in order to transmit/display over different media and networks.`
                * :hyellow:`You are responsible for the consequences of publishing the Content to Twitter *and* the consequences of Third Parties using it.`
                * :hyellow:`"You understand that your Content may be syndicated, broadcast, distributed, or published by our partners and if you do not have the right to submit Content for such use, it may subject you to liability"`
                * :hgreen:`Twitter is not responsible if it uses your content, and then if it is found that your content violates the previous terms.`
                * :hgreen:`It is assumed that you have the power to grant rights to content that you submit.`

        6. Your License to Use the Services
                * :hgreen:`You can use the service gratis, world wide, blah blah blah.`
                * :hblue:`[Marketing rubbish about Twitter being here for you to use for enjoyment (within Terms)]`

        7.  Twitter Rights
                * :hgreen:`Twitter owns Twitter.`
                * :hgreen:`Twitter is protected by laws of the US and by "other" countries.`
                * :hgreen:`You are not allowed to use Twitter's identity attributes (name, branding).`
                * :hgreen:`Any feedback, comments or suggestions may be used by Twitter, without obligation to you.`

        8. Restrictions on Content and Use of the Services
                * **Tip:** :hblue:`"Twitter does not disclose personally identifying information to third parties except in accordance with our Privacy Policy."`
                * **Tip:** :hgreen:`"We encourage and permit broad re-use of Content. The Twitter API exists to enable this."`
                * :hyellow:`Twitter may remove content.`
                * :hgreen:`Twitter may refuse content.`
                * :hyellow:`Twitter may suspend or terminate users. (I *think* they mean Accounts...)`
                * :hyellow:`Twitter may re-claim usernames, without liability to you.`
                * :hyellow:`Twitter reserves the right to read, access, preserve and disclose any information as needed to:`
                        * :hred:`"satisfy any applicable law", regulation, legal process or governmental request.` (not necessarily a *legal* request...)
                        * :hyellow:`enforce the the Terms, including for the reason of investigation thereof. (Guilty or not)`
                        * :hyellow:`"detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect the rights, property or safety of Twitter, its users and the public."`
                * :hyellow:`"you have to use the Twitter API if you want to reproduce, modify, create derivative works, distribute, sell, transfer, publicly display, publicly perform, transmit, or otherwise use the Content or Services."`
                * :hgreen:`You may not tamper with non-public areas of Twitter.`
                * :hgreen:`You may not probe any system of Twitter.` (Client-side software not mentioned)
                * :hgreen:`You may not circumvent security or authentication mechanisms of Twitter.`
                * :hgreen:`You must use official interfaces to search and access Twitter.` (no backdoors)
                * :hgreen:`Crawlers must respect the robots.txt`
                * :hyellow:`"scraping the Services without the prior consent of Twitter is expressly prohibited"` (ambiguous)
                * :hgreen:`No spoofing Twitter network communications.`
                * :hgreen:`No cracking or spamming of Twitter users.`

        9. Copyright Policy
                * :hyellow:`Twitter responds to Copyright/IP infringement notices.`
                * :hblue:`[details of what to provide in an infringement notice]`
                * :hred:`Twitter reserves the right to remove content *alleged* to be infringing without notice and without liability to you.` (Users can be bullied by copyright trolls.)
                * :hyellow:`Twitter may terminate the user account if determed to be a "repeat infringer".`

        10. Ending These Terms
                * Terms apply until terminated as follows:
                        * :hgreen:`You deactivate your account and discontinue use of Twitter services.` (Link to deactivation supplied, but **what about Twitter Services embedded within Third Parties where you have no choice?**)
                        * :hgreen:`Your accounts may be deactivated due to long inactivity under the Twitter inactivity policy.`
                        * :hyellow:`Twitter may suspend or terminate or cease providing you with any or all services at any time for any reason, including but not limited to:`
                                * :hblue:`Violation of Terms or "Twitter Rules"`
                                * :horange:`You create risk or legal exposure to Twitter.`
                                * :hyellow:`It is no longer commercially viable to provide service to you.`
                        * :hgreen:`Twitter will make "reasonable" attempt to notify you about the above the next time you attmept to access your account.`
                * :hred:`"In all such cases, the Terms shall terminate, including, without limitation, your license to use the Services, except that the following sections shall continue to apply: 4, 5, 7, 8, 10, 11, and 12."` (Uh, OK...)
                * :horange:`"Nothing in this section shall affect Twitter’s rights to change, limit or stop the provision of the Services without prior notice"` (So what was said here cannot be used in argument to other sections.)

        11. Disclaimers and Limitations of Liability
                * :hblue:`This section "limits the liability of Twitter and its parents, subsidiaries, affiliates, related companies, officers, directors, employees, agents, representatives, partners, and licensors (collectively, the "Twitter Entities")."`

                * :hgreen:`Services are available "As Is"` - there's no real need to go further into this.
                * :hgreen:`Links to Third Parties are provided with no guarantees or liabilities by Twitter.`
                * :hgreen:`Limitation of Liability - Twitter will protect itself using the full extent of applicable laws.`
                        * :hblue:`"IN NO EVENT SHALL THE AGGREGATE LIABILITY OF THE TWITTER ENTITIES EXCEED THE GREATER OF ONE HUNDRED U.S. DOLLARS (U.S. $100.00) OR THE AMOUNT YOU PAID TWITTER, IF ANY, IN THE PAST SIX MONTHS FOR THE SERVICES GIVING RISE TO THE CLAIM."` (Looks intresting. "You can't sue us into oblivion, lol.")
 
        12. General Terms
                * :horange:`Waiver and Severability - If Twitter cannot enforce a right or provision of the Terms it will not be deemed a waiver of the right and it will be eliminated or limited to the "extent necessary" and the remaining terms will remain intact.`
                * :hgreen:`Controlling Law and Jurisdiction - Terms and actions related will be govered by State of California laws. All legal activity will be brought from federal or state courts of San Francisco County, CA., US.`
                * :horange:`Entire Agreement - "These Terms, the Twitter Rules and our Privacy Policy are the entire and exclusive agreement between Twitter and you."` (So, actually, the rest of this T&C is in the "Twitter Rules" and "Privacy Policy" documents.)
                        * :horange:`Twitter may revise the terms but will only notify you at their discretion (by a Tweet on their profile, or email).`

        * :hgreen:`Effective: June 25, 2012`
        * :hgreen:`Archive of previous Terms provided.`

Privacy Policy
^^^^^^^^^^^^^^^
`Twitter's Privacy Policy can be found here... <https://twitter.com/privacy>`_

    * :hgreen:`"What you say on Twitter may be viewed all around the world instantly."` - really Twitter? Though maybe their point is that you can't just take it back.
    * :hgreen:`"This Privacy Policy describes how and when Twitter collects, uses and shares your information when you use our Services."` (The scope)

    **Information Collection and Use**

            * :hgreen:`Upon registation you provide a basic set of personal information. Minimal information is shown publicly (not your email address!) and some of this data is accessible without registration.` 

            **Additional Information**

            * :hgreen:`You may optionally provide Twitter with extra personal details (biography, location, an avatar) and a mobile number for SMS services.`
            * :hyellow:`Your contact information may be used by Twitter for marketing purposes and notifications.`
            * :hgreen:`You may opt out from notifications`
            * :hgreen:`You can control whether others can find your profile by providing contact information as a query.`
            * :hyellow:`You may upload your address book to have Twitter locate existing users based on that data.` (People in the address book can then be associated with your sociograph, unwillingly.)
            * :hgreen:`You may remove your uploaded address book from the service at any time.` (And from their backups?)
            * :hgreen:`If you email Twitter they will keep your message and contact details.`
            * :hyellow:`If you connect your Twitter account to your account on another service in order to cross-post between Twitter and that service, the other service may send us your registration or profile information on that service and other information that you authorize.` (Assuming the other service has `OAuth <http://oauth.net>`_ implemented correctly!)
                    * :hyellow:`The data retrieved from the other service is deleted from Twitter "within a few weeks".`
            * :hgreen:`Providing the additional information described in this section is entirely optional.`

            **Tweets, Following, Lists and other Public Information**

            * :hgreen:`"Most of the information you provide us is information you are asking us to make public."` (As long as it is obvious...)
            * :hyellow:`The metadata surrounding a Tweet is also made public.`
            * :hyellow:`The default is to make information public, but there are privacy controls *generally* available. Data remains public until it is deleted.` (Is data on the Internet ever truly deleted?)
            * :hyellow:`You public data is instantly made widely available because of indexing by search engines, SMS messaging, archiving by the US Library of Congress.`

            **Location Information**

            * :hyellow:`"You may choose to publish your location in your Tweets and in your Twitter profile. You may also tell us your location when you set your trend location on Twitter.com or enable your computer or mobile device to send us location information."`
            * :hgreen:`You may choose your location preferences for your account.`
            * :horange:`"We may use and store information about your location to provide features of our Services, such as Tweeting with your location, and to improve and customize the Services, for example, with more relevant content like local trends, stories, ads, and suggestions for people to follow."` (Advertising, no opt out?)

            **Links**

            * :hred:`"Twitter may keep track of how you interact with links across our Services, including our email notifications, third-party services, and client applications, by redirecting clicks or through other means. We do this to help improve our Services, to provide more relevant advertising, and to be able to share aggregate click statistics such as how many times a particular link was clicked on."` (Profiling, advertising... No mention of opt-out.)

            **Cookies**

            * :hyellow:`Log Data may include information such as your IP address, browser type, operating system, the referring web page, pages visited, location, your mobile carrier, device and application IDs, search terms, and cookie information.`
            * :hred:`"We receive Log Data when you interact with our Services"` (This includes when you load *any* page with a Twitter web asset or widget!)
            * :horange:`"...for Widget Data, we will either delete Log Data or remove any common account identifiers, such as your username, full IP address, or email address, after 18 months"` (18 months, or an eternity - whichever comes first)

            **Widget Data**

            * :hred:`"...websites first load our buttons or widgets for display, we receive Log Data, including the web page you visited and a cookie that identifies your browser ("Widget Data")."`
            * :horange:`"After a maximum of 10 days, we start the process of deleting or aggregating Widget Data, which is usually instantaneous but in some cases may take up to a week."` (Or, 18 months apparently)
            * :hyellow:`"Tailored content is stored with only your browser cookie ID and is separated from other Widget Data such as page-visit information"` (Basic anonymisation, but with up to 18 months analysis time this is useless as the data in the domain set is still mappable to the co-domain set.)
            * :horange:`"This feature is not available to all users yet" ... " If you want, you can suspend it or turn it off, which removes from your browser the unique cookie that enables the feature".` (But if you remove the cookie, you're likely to just get a new one? Does this function respect Do Not Track?)

            **Third Parties**

            * :hred:`Twitter uses many third party services to provide support functions *and analytics*` (Twitter embeds Google Analytics, which collects data on you.)
            * :hred:`"Third-party ad partners may share information with us, like a browser cookie ID or cryptographic hash of a common account identifier (such as an email address), to help us measure ad quality and tailor ads."` (So whatever information Twitter says they don't collect, they can still analyse by recieving it from a third party.)
            * :horange:`"If you prefer, you can turn off tailored ads in Twitter account settings so that your account is not matched to information shared by ad partners for tailoring ads"` (This gesture is pointless. This uses "Do Not Track" however it does not stop ads  from being served to you and then the resulting third parties doing analytics from the ads you load.)

    **Information Sharing and Disclosure**

            * **Tip:** :hred:`We do not disclose your private personal information except in the limited circumstances described here.` (We don't, but we do! Thanks.)
            
            **Your Consent**

            * :hgreen:`You may authorise a third-party application to access your account.`
            
            **Service Providers**

            * :hred:`Twitter uses "Service Provider" within the US and "abroad" and shares your *private* personal information with them.` ("abroad" - how rude! When you set things in your account to be private why do they assume it is OK to share with third parties?)
            * :hred:`"on the condition that the third parties use your private personal data only on our behalf and pursuant to our instructions."` (Oh the ironing is delicious!)

            **Law and Harm**

            * :hgreen:`"Notwithstanding anything to the contrary in this Policy, we may preserve or disclose your information if we believe that it is reasonably necessary to comply with a law, regulation or legal request; to protect the safety of any person; to address fraud, security or technical issues; or to protect Twitter's rights or property"` (standard, really.)
            * :hgreen:`"...nothing in this Privacy Policy is intended to limit any legal defenses or objections that you may have to a third party’s, including a government’s, request to disclose your information."`

            **Business Transfers**

            * :hyellow:`If Twitter is transferred to a different entity, your data goes with it (it may be sold!)`
            * :hgreen:`Transfer of Twitter means transfer of this privacy policy.`

            **Non-Private or Non-Personal Information**

            * :hgreen:`Twitter many disclose non-private information about you including tweets, links and associated metadata.`

    **Modifying Your Personal Information**

            * :hgreen:`You may modify your personal information associated with your account.`
            * :hgreen:`You may delete your account.`
            * :hyellow:`Upon deletion, your account may be recoverable for 30 days.`

    **Our Policy Towards Children**

            * :hgreen:`Twitter is not aimed at children under 13.`
            * :hgreen:`If you become aware that a child has provided personal information without consent you may contact Twitter to resolve the issue.`
            * :hgreen:`Twitter does not want personal information on children under 13. When discovered, steps will be taken to remove the information and terminate the child's account.`

    **EU Safe Harbor Framework**

            * :hgreen:`Twitter complies with the EU Safe Harbor Framework which is intended to guide US companies in storing private information in a EU law compliant way.`

    **Changes to this Policy**

            * :hgreen:`The policy will occasionally be revised. It will remain available at http://twitter.com/privacy`
            * :hyellow:`You may be notified (at Twitter's discresion) when changes to the policy are made by either email or an tweet on Twiter's profile.`
            * :hyellow:`By continuing to use Twitter after the policy update, you agree to the policy.` (What if you didn't see the damn Tweet from @Twitter?)

    * :hgreen:`Effective: July 3, 2013`

    * :hgreen:`Archive of previous Privacy Policies available`

Security Policy
^^^^^^^^^^^^^^^
Twitter's `security policy document <https://twitter.com/about/security>`_ is not a policy document in the same way the others are.
It simply says that Twitter "takes steps every day to provide a secure Twitter experience for our users" and does not say anything about their policy on reacting to security issues.
Twitter provides forms for people who notice security problems to fill out.

It is interesting to note that they thank the White Hats/Penetration Testers on that page - but at the same time most of those people likely violated the ToS while researching the vulnerabilities.

Decoding the T&C, Privacy Policy
--------------------------------
Twitter's "Tip" notes in the T&C are nice but it is too easy to assume that the tip is actually a summary of the section! Clearly from this summary you can tell that the devils are clearly in the details. Without writing a novella, I couldn't even begin to explore the intricacies involved in each term and the interactions between each term. The best I could do was to just write a few snarky comments next to some of the terms.

The formatting and language of Twitter's Terms and Conditions document is almost certainly above average in terms of readability and clarity, however Twitter has obscured some potentially *very* important details into separate "Twitter Rules" and "Privacy Policy" documents. 

While reading the T&C document it struck me on how important it is to realise that Terms and Conditions *are not the same as* the Privacy Policy. 

Twitter's Privacy Policy has two personalities. The first personality is the *"hey, you wanted this stuff to be public anyway!"* which I have no problem with - but then there's the parts where you see Twitter's often questioned lack of business model come into play. Twitter's response to their lack of a business model was to partner with third party advertising networks for the purpose of making money from their user's traffic and demographic/behavioral data.

As to answer the question on my general opinion of Twitters Terms and Conditions, I would say that while they are not completely terrible they are indeed a typical example of what I expect from a *legacy* social network. I think the main issue I have with the T&C is my inability to opt out of sharing content with Twitter partners, so effectively the lack of control over sub-licensing of my content.
