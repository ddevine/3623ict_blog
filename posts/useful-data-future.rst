.. link: 
.. description: The Semantic Web, The Internet of things. What are they and where is the web going?
.. tags: Semantic Web, Internet of Things, Web 3.0, Web.Next
.. date: 2013/08/16 14:00:33
.. title: Useful Data is the Future
.. slug: useful-data-future

.. role:: strike
    :class: strike

The Internet is a series of tubes... That pour data into silos to be stored indefinitely and never looked at again. The Semantic Web looks to help fix this problem by making data more useful, often in hand with movements such as The Internet of Things and decentralisation and federation projects.

.. figure:: /blog_images/simondseconoart-small.jpg
        :alt: Web 2.0 services have created a series of walled gardens/silos.
        :align: center

        Web 2.0 services have created a series of walled gardens and data silos (`Source <http://www.w3.org/DesignIssues/diagrams/social/simondseconoart-small.png>`_).

But what is wrong with Web 2.0? A somewhat cynical answer is that the Web 2.0 era is the Internet's equivalent of the Baby Boomer era -- great, but from a time bygone and now we have to hoist our breeches and move on. We have a lot of work to do before progressing into the next era and beyond.

.. TEASER_END

Web.Next
--------

Firstly I'd like to say that even contemplating versioning the Internet is impractical and fundamentally wrong. The folks behind the Chrome(ium) and Firefox browsers have already come to this conclusion and that is why they no longer follow a strict a slow and strict mega-version type release model but rather a rapid fire update model where version numbers are invisible to the end users. The Web is an ever-changing platform, spurred into change by a constant barrage of evolving use-cases and producer/consumer technologies, that is the reality of the modern Internet - a platform for the stream of global human consciousness. If you were to try to put the Web into a box, you'd instantly find that it has already grown too large or misshapen for the box you just made. Web 3.0 is not going to come to those who just wait, it will come to those who make it. From this point on *the term Web 3.0 should be banned*. And thus, the replacement: *Web.Next*

Bagginses? What is a Bagginses, Precious?
-----------------------------------------

.. figure:: http://www.howtobeadad.com/wp-content/uploads/2012/12/mystified.jpg
        :align: right

Defining Web.Next is an abstract task because there is no clearly defined window of time or set of deliverables which encase some sort of release version. However, it seems to be generally accepted that the major linchpin is the idea of The Semantic Web.

The Semantic Web is a movement to create common data formats for storage and exchange of information between people and computers, or in other terms: the idea that we should express the semantic meaning of our data in a way designed to *allow machines to understand the data as more useful information*. At the moment we have an ever growing collection of data, but very little of it can be easily interpreted as information without the tricky and error prone process of `Natural Language Processing <https://en.wikipedia.org/wiki/Natural_language_processing>`_. 

In hand with the Semantic Web is the idea of The Internet of Things. Network connected sensors placed pervasively throughout a physical environment can record useful data which can be shared, analysed or even immediately acted upon. This data can be recorded in a semantically meaningful format - expressing the who, what, when, where and why in data which can be very useful, as in the following.

Another key point of Web.Next is the movement towards customisability on an individual basis. The mechanics of customisation comes down to trivially simple code, generally, but the challenge is in building a picture of the user's context. The phrase "context is king" is the modern web designer's mantra (and oh-by-the-way I'd love to have a rant on the quackery that is interface *"design"* sometime soon). The analysis of data to determine contexts is where semantically complete data is extremely useful. This data may be collected from the Internet of Things, behavioral analysis - perhaps using data from several different sources, or from crawling/indexing of the web. Customisation can also come in the form of the providing the ability to interact with services via the web based APIs which have gradually grown to be pervasive as the web 2.0 ecosystem has evolved. These web APIs allow people to interact with services through client interfaces of their own making.

Of course, all of these things generally involve a *lot* of data and so the term *Big Data* has been coined to refer to the storage and analysis of enormous data sets. It is the stuff of dreams for the `business intelligence <https://en.wikipedia.org/wiki/Business_intelligence>`_ crowd due to the endless reams of impressive looking reports they can generate with such data at their disposal. 

Why Do We Want More Stuff?
--------------------------

Simply, we want all this new stuff because we can do crazy science fiction/next-level things with it.

Scientific research can be greatly accelerated if we can get computers to do much of the heavy lifting for us. The more meaningful data that can be accessed the greater the number of possible analysis can be run due to the flexibility of the interpretation of the data and the exponentially growing number of possible integration points between data sets. Utilising Machine Learning techniques to answer difficult problems with large data sets becomes much simpler due to information being much easier and more accurately extracted from the data. Perhaps through advances in the Artificial Intelligence and Natural Language Processing you may eventually be able to simply ask a computer a question and receive a meaningful answer. This would be made possible by the semantic web because computational algorithms will be able to find and utilise the data needed to create a graph of the universe of discourse and then apply (and hopefully adapt) analysis techniques to solve the problem in question.

Already, `IBM's Watson AI <http://en.wikipedia.org/wiki/Watson_%28computer%29>`_ was able to participate and *win* a game of Jeopardy against former winners. Jeopardy is a game of quite tricky natural langage based questions and answers that require a large amount of backing knowledge and understanding of context. Of course, Watson was specifically tuned for the task but in the future it is certainly conceivable that such systems could be auto-tuning and capable of a wider range of question and answer styles.

The implications for E-Health are exciting, as the Internet of Things can include health monitoring devices to collect data for health-care systems which could provide your doctors with information and an enhanced `clinical decision support system <http://en.wikipedia.org/wiki/Clinical_decision_support_system>`_ (which IBM's Watson is already being adapted for) to help treat existing problems in a very agile manner and to more effectively leverage preventative medicine for better (and cheaper) patient outcomes.

A little more closer to the current reality, Web.Next technologies mean that computers will be able to *know what you mean* regardless of *what you actually said* in a much more reliable manner. This could be integrated into existing things such as predictive typing to accurate and helpful suggestions when researching a topics. Even adapting interfaces on the fly to be *actually intuitive* so that you're not not locked into some crackpot designer's ideals of what usability is, will be possible.

As our modern and increasingly refined Internet grows more pervasive we are also finding that people are embracing the emerging Internet culture. Web.Next technologies help integrate virtual reality with reality. Without geographical and cultural bounds and the benefits of various view-points and the collective knowledge of humanity available at out whim means that social and technological advancement are about to explode in hopefully a most wonderful way.

Here Be Dragons
---------------
Of course, there are those who always want to spoil it for everybody. Increasingly, we are having to battle against privacy issues created by private, corporate and government entities. As is pointed out by `Eben Moglen <http://en.wikipedia.org/wiki/Eben_Moglen>`_ in the following video, this is nothing new - for hundreds of years various entities have been battling against the `freedom of thought <http://en.wikipedia.org/wiki/Freedom_of_thought>`_ but this time it is even worse. Unlike before we have media that watches us watching it. People no-longer have to be pressed to inform on their friends because they willingly do so on social media and via the sociograph that people project. 

.. youtube:: sKOk4Y4inVY

The global consciousness that is being developed is in grave danger of being abused by malicious parties. The greatest problem so far is that we are ignoring the warning signs that are telling as we are engaging in bad design. When you consider the part that the web is playing in the development of humanity the stakes have never been higher, because the way we are going we may lose our ability to have free thought.

I really cannot recommend watching the above presentation by Eben Moglen enough, as it gives you an honest, interesting, coherent and detailed view on current and future privacy issues on the web. 

:strike:`Web 4.0` Web.Next + 1
-------------------------------

And so the solution to the woes of scalability, privacy and equality is not simple - but definitely not complex. I think the solution revolves around three main points and possibly a fourth player to spice things up.

1. Distributed systems
2. Federated systems
3. Open technologies


When you join the 3 points together you get distributed and federated systems which are scalable, customisable and put users back in control of their data. A few months ago I drafted an article about `distributed and federated social networks which you can view here. </documents/fedsocialnet.pdf>`_

4. "Unhosted" systems

The fourth player I see is the idea of *unhosted* systems. These are systems where the users keep hold of their data (locally or in a controllable and convenient network location) at all times and they can choose and verify how and what applications (potentially served over the web) do with their data and potentially control any outgoing communications. To read more about the idea of unhosted systems you can check out `unhosted.org <http://unhosted.org/>`_.
